object Utils {
  @SuppressWarnings(
    Array("org.wartremover.warts.Equals",
          "org.scalastyle.scalariform.MethodNamesChecker"))
  implicit final class AnyOps[A](self: A) {
    def ===(other: A): Boolean = self == other
  }
}
