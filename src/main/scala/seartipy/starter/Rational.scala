package seartipy.starter

import seartipy.starter.Main.gcd

class Rational(num: Int, den: Int) {
  var g = gcd(num, den)
  val numerator: Int = num / g
  val denominator: Int = den / g

  def +(that: Rational): Rational =
    new Rational(
      (numerator * that.denominator) + (denominator * that.numerator),
      denominator * that.denominator)
  def -(that: Rational): Rational =
    new Rational(
      (numerator * that.denominator) - (denominator * that.numerator),
      denominator * that.denominator)
  def *(that: Rational): Rational =
    new Rational(
      (numerator * that.numerator), denominator * that.denominator)
  def /(that: Rational): Rational =
    new Rational((numerator * that.denominator), denominator * that.numerator)
}
