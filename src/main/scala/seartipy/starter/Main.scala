package seartipy.starter

object Main extends App {
  def min2(x: Int, y: Int): Int = if (x > y) x else y

  def gcd(x: Int, y: Int): Int = {
    var n = min2(x, y)
    if(n == 0)
      return 1
    while (n > 0) {
      if (x % n == 0 && y % n == 0)
        return n
      n -= 1
    }
    -1
  }
}
