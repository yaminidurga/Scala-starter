package seartipy.starter

class Account (val id:Int,private var balance:Money){
  def withDraw(amount:Money):Boolean = {
    if(amount.compareTo(balance) <= 0){
      balance -= amount
      true
    }
    else {
      false
    }
  }
  def deposit(amount:Money):Unit = {
    balance += amount
  }
  def checkBalance:Money = balance
}
