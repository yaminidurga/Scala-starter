package seartipy.starter
class Node(var data:Int, var next:Node)

class SingleLinkedList {
  private var head :Node = null
  private var length:Int = 0

  def first: Int = head.data
  def size: Int = length

  def addFirst(value:Int):Unit= {
    val node = new Node(value,head)
      node.next = head
      head = node
      length += 1
  }
  def removeFirst():Int = {
    val result = head.data
    head = head.next
    length -= 1
    result
  }
  def insertAfter(value:Int,at:Node):Unit={
    val node = new Node(value,at.next)
    at.next = node
    length += 1
  }
  def removeAfter(node:Node):Unit = {
    require(node.next != null)
    node.next = node.next.next
    length -= 1
  }
  def nodeAt(index:Int):Node = {
    var i = 0
    var temp = head
    while(temp != null) {
      if (i == index) {
        return temp
      }
      temp = temp.next
      i += 1
    }
    null
  }
  def toArray:Array[Int] = {
    var current = head
    val arr = new Array[Int](length)
    var i = 0
    while(current != null){
      arr(i) = current.data
      current = current.next
      i += 1
    }
    arr
  }
}


















































