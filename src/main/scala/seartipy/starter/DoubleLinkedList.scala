package seartipy.starter

class DoubleNode(var prev:DoubleNode,var data:Int, var next:DoubleNode)

class DoubleLinkedList {
  private var head: DoubleNode = null
  private var tail: DoubleNode = null
  private var length = 0

  def first(): Int = head.data

  def last(): Int = tail.data

  def size(): Int = length

  def addFirst(value: Int): Unit = {
    val newNode = new DoubleNode(null, value, head)
    val first = head
    if (first == null) {
      head = newNode
      tail = newNode
    } else {
      first.prev = newNode
      head = newNode
    }
    length += 1
  }

  def toArray: Array[Int] = {
    var current = head
    val arr = new Array[Int](length)
    var i = 0
    while (current != null) {
      arr(i) = current.data
      current = current.next
      i += 1
    }
    arr
  }

  def addLast(value: Int): Unit = {
    val newNode = new DoubleNode(tail, value, null)
    val last = tail
    if (last == null) {
      head = newNode
      tail = newNode
    } else {
      last.next = newNode
      tail = newNode
    }
    length += 1
  }

  def insertAt(at: DoubleNode, value: Int): Unit = {
    val newNode = new DoubleNode(at.prev, value, at)
    if (at.prev != null)
      at.prev.next = newNode
    at.prev = newNode
    if (at == head)
      head = newNode
    length += 1
  }

  def nodeAt(index:Int):DoubleNode = {
    require(index < size())
    var i = 0
    var temp = head
    while(temp != null){
      if(i == index)
        return temp
      temp = temp.next
      i += 1
    }
    null
  }
  def removeFirst():Int={
    val first = head
    require(first != null)
    first.next.prev = null
    val result = first.data
    head = first.next
    length -= 1
    result
  }
  def removeLast():Int = {
    val first = tail
    require(first != null)
    first.prev.next = null
    val result = first.data
    tail = first.prev
    length -= 1
    result
  }
  def findAt(value:Int):DoubleNode = {
    var temp = head
    while(temp != null){
      if(temp.data == value)
        return temp
      temp = temp.next
    }
    null
  }
}













