package seartipy.starter

class Complex(val real: Rational, val imaginary: Rational) {
  def +(that: Complex): Complex =
    new Complex((real + that.real), imaginary + that.imaginary)
  def -(that: Complex): Complex =
    new Complex((real - that.real), imaginary - that.imaginary)
  def *(that: Complex): Complex =
    new Complex((real * that.real), imaginary * that.imaginary)
  def /(that: Complex): Complex =
    new Complex(
      (real * that.real) + (real * that.imaginary) + (imaginary * that.real) - (imaginary * that.imaginary),
      (real * that.real) - (imaginary * that.imaginary))

}


