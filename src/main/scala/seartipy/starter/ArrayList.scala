package seartipy.starter

class ArrayList {
  private var arr:Array[Int] = Array(10)
  private var length: Int = 0

  def size(): Int = length
  def capacity(): Int = arr.length
  def first: Int = arr(0)
  def last: Int =arr(length - 1)

  def get(i: Int): Int = arr(i)
  def set(value: Int, i: Int):Unit = arr(i)= value


  def ensureCapacity(newCapacity: Int):Unit = {
    if (arr.length > newCapacity) {
      return
    }
  def copy(src:Array[Int],dest:Array[Int]):Unit = {
    for (i <- src.indices)
      dest(i) = src(i)
  }

    val temp = Array[Int] (capacity() * 2 )
    copy(arr, temp)
    arr = temp
  }

  def add(value: Int): Unit = {
    ensureCapacity(length + 1)
    arr(length) = value
    length += 1
  }

  def isEmpty(): Boolean = length == 0

  def pop(): Int = {
    require(length != 0)
    length -= 1
    arr(length)
  }

  def insert(value: Int, at: Int):Unit = {
    ensureCapacity(arr.length)
    for (i <- at to length ) {
      arr(i + 1) = arr(i)
    }
    arr(at) = value
    length += 1
  }

  def remove(at: Int): Int = {
    ensureCapacity(arr.size)
    val temp = arr(at)
    for (i <- at to length)
      arr(i) = arr(i + 1)
    length -= 1
    temp
  }
}
























































