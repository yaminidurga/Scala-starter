package seartipy.starter

class Money(_dollar:Int = 0, _cent:Int = 0) {
  private def this(totalCents :Int ) =this(totalCents / 100,totalCents % 100)

  private val totalCents = _dollar * 100 +_cent
  var dollar:Int = totalCents / 100
  var cent:Int = totalCents % 100

  def +(that:Money):Money = new Money(totalCents + that.totalCents)
  def -(that:Money):Money = new Money(totalCents - that.totalCents)
  def *(n:Int):Money= new Money(n * totalCents)
  def /(n:Int):Money = new Money(n / totalCents)
  def compareTo(that:Money):Int = totalCents - that.totalCents

}
