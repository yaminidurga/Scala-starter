package seartipy.starter.suites

import org.scalatest._
import seartipy.starter.Rational

class RationalSuite extends FunSuite with Matchers{
  test("constructor") {
    val r = new Rational(1, 2)
    assert(r.numerator === 1)
    assert(r.denominator === 2)

    val r1 = new Rational(6, 8)
    assert(r1.numerator === 3)
    assert(r1.denominator === 4)
  }
  test("add"){
    val r = new Rational(1,2)
    val r1 = new Rational(1,1)
    val r2 = r + r1
    assert(r2.numerator === 2)
    assert(r2.denominator === 3)

  }
  test("sub"){
    val r = new Rational(1,2)
    val r1 = new Rational(3,1)
    val r2 = r1 - r
    assert(r2.numerator === 5)
    assert(r2.denominator === 2)
  }
  test("mul"){
    val r = new Rational(2,2)
    val r1 = new Rational(2,1)
    val r2 = r * r1
    assert(r2.numerator == 2)
    assert(r2.denominator == 1)
 }
  test("div"){
    val r = new Rational(1,2)
    val r1 = new Rational(2,3)
    val r2 = r / r1
    assert(r2.numerator == 3)
    assert(r2.denominator == 4)
  }

}













