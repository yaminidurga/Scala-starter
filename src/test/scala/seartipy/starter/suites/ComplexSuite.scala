//package seartipy.starter.suites
//
//import org.scalatest.{FunSuite, Matchers}
//import seartipy.starter.{Complex, Rational}
//
//class ComplexSuite extends FunSuite with Matchers {
//  test("add") {
//    val c = new Complex(new Rational(1, 2), new Rational(3, 4))
//    val c1 = new Complex(new Rational(1, 1), new Rational(2, 4))
//    val c2 = c + c1
//    assert(c2.real.numerator === 3)
//    assert(c2.real.denominator === 2)
//    assert(c2.imaginary.numerator === 5)
//    assert(c2.imaginary.denominator === 2)
//  }
//  test("sub"){
//    val c = new Complex(new Rational(4, 2), new Rational(3, 2))
//    val c1 = new Complex(new Rational(2, 1), new Rational(6, 4))
//    val c2 = c - c1
//    assert(c2.real.numerator === 1)
//    assert(c2.real.denominator === 1)
//    assert(c2.imaginary.numerator === 0)
//    assert(c2.imaginary.denominator === 4)
//  }
//  test("multiply"){
//    val c = new Complex(new Rational(2, 2), new Rational(3, 2))
//    val c1 = new Complex(new Rational(2, 1), new Rational(2, 4))
//    val c2 = c * c1
//    assert(c2.real.numerator === 3)
//    assert(c2.real.denominator === 2)
//    assert(c2.imaginary.numerator === 1)
//    assert(c2.imaginary.denominator === 1)
//  }
//  test("divide"){
//    val c = new Complex(new Rational(5, 2), new Rational(3, 2))
//    val c1 = new Complex(new Rational(3, 2), new Rational(1, 2))
//    val c2 = c / c1
//    assert(c2.real.numerator === 5)
//    assert(c2.real.denominator === 3)
//    assert(c2.imaginary.numerator === 7)
//    assert(c2.imaginary.denominator === 1)
//  }
//}
