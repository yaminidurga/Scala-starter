package seartipy.starter.suites

import org.scalatest.{FunSuite, Matchers}
import seartipy.starter.DoubleLinkedList

class DoubleLinkedListuite extends FunSuite with Matchers {
  test("addfirst"){
    val list = new DoubleLinkedList()
    list.addFirst(10)
    assert(list.size() === 1)
    assert(list.toArray === Array(10))
    list.addFirst(20)
    assert(list.toArray === Array(20,10))
    list.addFirst(30)
    assert(list.size() === 3)
    assert(list.toArray === Array(30,20,10))

  }
  test("addLast"){
    val list = new DoubleLinkedList()
    list.addLast(100)
    list.addFirst(10)
    list.addLast(200)
    assert(list.size() === 3)
    assert(list.toArray === Array(10,100,200))
    list.addFirst(20)
    list.addLast(40)
    assert(list.toArray === Array(20,10,100,200,40))
    assert(list.size() === 5)
  }

  test("insertAt"){
    val list = new DoubleLinkedList()
    list.addFirst(100)
    list.addLast(200)
    list.addLast(300)
    assert(list.toArray === Array(100,200,300))
    assert(list.size() === 3)
    list.insertAt(list.nodeAt(1), 30)
    assert(list.nodeAt(1).data === 30)
    assert(list.toArray === Array(100,30,200,300))
    assert(list.size() === 4)
    assert(list.nodeAt(3).data === 300)
    assert(list.toArray === Array(100,30,200,300))
    assert(list.size() === 4)
    list.insertAt(list.nodeAt(2),11)
    assert(list.toArray === Array(100,30,11,200,300))
    assert(list.size() === 5)
    list.insertAt(list.nodeAt(0),22)
    assert(list.toArray === Array(22,100,30,11,200,300))
    assert(list.size() === 6)
  }
  test("nodeAt") {
    val list = new DoubleLinkedList()
    list.addLast(200)
    list.addFirst(10)
    list.addLast(600)
    assert(list.nodeAt(0).data === 10)
    assert(list.toArray === Array(10,200,600))
    assert(list.size() === 3)
    assert(list.nodeAt(2).data === 600)
  }
  test("removeFirst"){
    val list = new DoubleLinkedList()
    list.addFirst(10)
    list.addFirst(20)
    list.addFirst(30)
    list.addLast(40)
    assert(list.toArray === Array(30,20,10,40))
    assert(list.size() === 4)
    assert(list.removeFirst() === 30)
    assert(list.toArray === Array(20,10,40))
    assert(list.size() === 3)
    list.removeFirst()
    assert(list.toArray === Array(10,40))
    assert(list.size() === 2)
  }
  test("removeLast"){
    val list = new DoubleLinkedList()
    list.addFirst(10)
    list.addFirst(20)
    list.addFirst(30)
    list.addLast(40)
    assert(list.toArray === Array(30,20,10,40))
    assert(list.size() === 4)
    assert(list.removeLast() === 40)
    assert(list.toArray === Array(30,20,10))
    assert(list.size() === 3)
    list.removeLast()
    assert(list.toArray === Array(30,20))
    assert(list.size() === 2)
  }
}
