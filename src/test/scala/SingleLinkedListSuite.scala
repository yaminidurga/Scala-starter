import org.scalatest.{FunSuite, Matchers}
import seartipy.starter.SingleLinkedList

class SingleLinkedListSuite extends FunSuite with Matchers {
  test("addFirst") {
    val list = new SingleLinkedList()
    list.addFirst(10)
    list.addFirst(20)
    assert(list.size === 2)
    assert(list.toArray === Array(20,10))
    list.addFirst(30)
    list.addFirst(40)
    assert(list.toArray === Array(40,30,20,10))
    assert(list.size === 4)
  }
  test("removeFirst"){
    val list = new SingleLinkedList()
    list.addFirst(10)
    list.addFirst(20)
    assert(list.size === 2)
    assert(list.toArray === Array(20,10))
    list.addFirst(30)
    list.addFirst(40)
    assert(list.toArray === Array(40,30,20,10))
    assert(list.size === 4)
    list.removeFirst()
    assert(list.size === 3)
    assert(list.toArray === Array(30,20,10))

  }
  test("nodeAt") {
    val list = new SingleLinkedList()
    list.addFirst(200)
    list.addFirst(10)
    list.addFirst(600)
    assert(list.nodeAt(0).data === 600)
    assert(list.toArray === Array(600,10,200))
    assert(list.size === 3)
    assert(list.nodeAt(2).data === 200)
  }
  test("insertAfter"){
    val list = new SingleLinkedList()
    list.addFirst(200)
    list.addFirst(10)
    list.addFirst(600)
    assert(list.toArray === Array(600,10,200))
    assert(list.size === 3)
    list.insertAfter(40, list.nodeAt(0))
    assert(list.toArray === Array(600,40,10,200))
    assert(list.size === 4)

  }
  test("removeAfter"){
    val list = new SingleLinkedList()
    list.addFirst(200)
    list.addFirst(10)
    list.addFirst(600)
    assert(list.toArray === Array(600,10,200))
    assert(list.size === 3)
    list.removeAfter(list.nodeAt(0))
    assert(list.toArray === Array(600,200))
    assert(list.size === 2)

  }

}




